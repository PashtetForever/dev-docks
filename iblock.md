## Удалить фото у раздела

```php
$section = new CIBlockSection();
$section->Update(SECTION_ID, ['PICTURE' => ['del' => 'Y']]);
```

## Проставить разделу фото от самого дорого товара в этом разделе
```php
$elements = ElementTable::getList([
        'select' => ['ID', 'IBLOCK_ID', 'DETAIL_PICTURE'],
        'filter' => [
            'IBLOCK_ID' => IBLOCK_ID,
            'IBLOCK_SECTION_ID' => $section['ID'],
            '!DETAIL_PICTURE' => false
        ]
    ])->fetchAll();
    $photos = [];
    foreach ($elements as $element) {
        $photos[$element['ID']] = $element['DETAIL_PICTURE'];
    }

    $prices = [];
    foreach ($elements as $element) {
        $price = \Bitrix\Catalog\PriceTable::getList([
            'select' => ['PRICE', 'ID'],
            'filter' => [
                'CATALOG_GROUP_ID' => 1,
                'PRODUCT_ID' => $element['ID']
            ]
        ])->fetch()['PRICE'];

        $prices[$element['ID']] = $price;
    }

    $topPriceId = array_search(max($prices), $prices);

    SectionTable::update($section['ID'], [
        'PICTURE' => CFile::CopyFile($photos[$topPriceId])
    ]);
```

## Проставить фото разделу первого уровнять от случайного раздела второго уровня
```php
$sections = SectionTable::getList([
    'select' => ['ID', 'IBLOCK_ID'],
    'filter' => [
        'IBLOCK_ID' => IBLOCK_ID,
        'DEPTH_LEVEL' => 1,
        'PICTURE' => false
    ]
])->fetchAll();

foreach ($sections as $section) {
    $sectionWithPhoto = SectionTable::getList([
        'select' => ['ID', 'IBLOCK_ID', 'NAME', 'PICTURE'],
        'filter' => [
            'IBLOCK_ID' => IBLOCK_ID,
            'DEPTH_LEVEL' => 2,
            '!PICTURE' => false,
            'IBLOCK_SECTION_ID' => $section['ID']
        ],
        'limit' => 1
    ])->fetch();

    $newPicture = CFile::CopyFile($sectionWithPhoto['PICTURE']);

    if($newPicture != 0){
        SectionTable::update($section['ID'], [
            'PICTURE' => $newPicture
        ]);
    }
}
```