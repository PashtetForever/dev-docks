## Оглавление из заголовков статьи
Парсим содержимое страницы и получаем оглавление
```php
$count = 0;
$detailText = preg_replace_callback(
'(<h(\d)>)',
function ($matches) use (&$count) {
    $count++;
    return "<h{$matches[1]} id='title-block-$count'>";
},
$arResult['DETAIL_TEXT']
);

preg_replace_callback(
    '(<h(\d) id=\'(.*?)\'>(.*?)</h)',
    function ($matches) use (&$titles) {
        $level = $matches[1];

        $titles[] = [
            'LEVEL' => $level,
            'ID' => $matches[2],
            'TITLE' => $matches[3]
        ];
    },
    $detailText
);
$arResult['DETAIL_TEXT'] = $detailText;
```
Верстка оглавления
```php
<div class="title-content">Содержание</div>
<ul class="titles-content">
    <?foreach ($titles as $title) { ?>
        <? if ($title['LEVEL'] > $depth) {echo "<li><ul>";$depth = $title["LEVEL"];} ?>
        <? if ($title['LEVEL'] < $depth) {echo "</ul></li>";$depth = $title["LEVEL"];} ?>
        <li><a href="#<?=$title['ID']?>"><i class="fa fa-angle-right"></i><?=$title['TITLE']?></a></li>
    <?} ?>
</ul>
```

Скролл к элементу по оглавлению
```js
    function scrollToElement(element, marginTop, speed) {
        if ($(element).length !== 0) { // проверим существование элемента чтобы избежать ошибки
            $('html').animate({ scrollTop: $(element).offset().top - marginTop }, speed); // анимируем скроолинг к элементу scroll_el
        }
    }

    $('.titles-content a, .button-link').on('click', function (e) {
        e.preventDefault();
        scrollToElement($(e.currentTarget.hash), 75, 500);
    });
```
